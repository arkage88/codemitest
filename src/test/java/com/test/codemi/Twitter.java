package com.test.codemi;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

import junit.framework.Assert;

public class Twitter {
	
	WebDriver driver = new ChromeDriver();
	WebDriverWait wait=new WebDriverWait(driver, 20000);
	
  @Test
  public void a_wrong_user_password() throws InterruptedException {
	  
		System.setProperty("webdriver.chrome.driver", "chromedriver.exe");
	      driver.manage().deleteAllCookies();
		  driver.manage().timeouts().pageLoadTimeout(40, TimeUnit.SECONDS);
		  driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		
		  driver.get("https://twitter.com/login");
		  
		  driver.findElement(By.name("session[username_or_email]")).sendKeys("arkagege");//email yang sudah ada
		  driver.findElement(By.name("session[password]")).sendKeys("Secret123");
		  
		  driver.findElement(By.cssSelector(".r-13qz1uu:nth-child(3) .css-18t94o4 > .css-901oao")).click();
		  
		  String actual2 = driver.findElement(By.cssSelector(".r-daml9f > .css-901oao")).getText();
			String expectedText2 = "The username and password you entered did not match our records. Please double-check and try again.";
			Assert.assertEquals(actual2,expectedText2);
		  
	  }
  
  @Test
  public void b_right_user_password() throws InterruptedException {
	  
		
	  	  driver.findElement(By.name("session[username_or_email]")).clear();
		  driver.findElement(By.name("session[username_or_email]")).sendKeys("@testarka2");//email yang sudah ada
		  driver.findElement(By.name("session[password]")).sendKeys("Secret123");
		  driver.findElement(By.name("session[password]")).sendKeys(Keys.ENTER);
		  
		  driver.findElement(By.cssSelector(".public-DraftStyleDefault-block")).sendKeys("Semoga masuk ke Codemi!!");
		  driver.findElement(By.xpath("//input[@type='file']")).sendKeys("E:\\smiley-face.jpg");
		  
		  driver.findElement(By.cssSelector(".css-18t94o4:nth-child(4) > .css-901oao > .css-901oao > .css-901oao")).click();
		  
//		  driver.findElement(By.xpath("//div[@id='react-root']/div/div/div[2]/main/div/div/form/div/div[3]/div/div")).click();
		  
		 
		  
	  }
}
